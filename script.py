import json
import yt_dlp
import os

URL = os.getenv('URL')

# ℹ️ See help(yt_dlp.YoutubeDL) for a list of available options and public functions
ydl_opts = {}
with yt_dlp.YoutubeDL(ydl_opts) as ydl:
    info = ydl.extract_info(URL, download=False)

os.system('git config user.name "GitHub Actions Bot"')
os.system('git config user.email "<>"')

for entry in info['entries']:
    json_obj = json.dumps({'id': entry['id'], 'title': entry['title'], 'description': entry['description'], 'published': entry['upload_date']})
    with open("episode.json", "w") as file:
        file.write(json_obj)
    
    # Commit changes
    os.system(f'git add episode.json')
    # Commit message and push
    os.system(f'git commit -m "{entry["id"]}" && git push')
